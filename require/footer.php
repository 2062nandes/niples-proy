<footer class="page-footer codrops-footer col s12">
      <div class="fot">
            <div  class="col s3 m3">
              <div class="fwrap">
                <h3>MENÚ PRICIPAL</h3>
                <ul id="datos" style="text-align:justify;">
                  <div class="col s6">
                    <li><a href="#inicio">Inicio</a></li>
                    <li><a href="#nosotros">Nosotros</a></li>
                    <li><a href="#contacto">Contactos</a></li>
                  </div>
                  <div class="col s6">
                    <li><a href="#niples"> NIPLES</a></li>
                    <li><a href="#resortes"> RESORTES</a></li>
                  </div>
                </ul>
              </div>
            </div>
            <div class="col s6">
                <div class="fwrap">
                    <h3>DIRECCIÓN SANTA CRUZ-BOLIVIA</h3>
                    <div id="datos">
                      <p>Av. Gricotá # 679, 3º Anillo Interno</p>
                      <p><i style="top:10px" class="material-icons">settings_phone</i> Telf.: 3507554 <span class="opt"> &nbsp;&nbsp;&nbsp;</span> <i style="font-size: 1.2em" class="fa fa-mobile"></i>Cels.: 72192169 - 78038025</p>
                      <p><i class="material-icons">email</i> niplestock@hotmail.com <a href="https://www.google.com/maps/d/viewer?mid=15zX_909H0FfLn0DXXzr6rcr09uM&hl=es" target="_blank"><i class="material-icons">room</i> Ver mapa</a></p>
                    </div>
                </div>
            </div>
            <div class="col s3">
                <div class="fwrap">
                    <h1>NIPLE STOCK</h1>
                    <div id="datos">
                    <p>Visitenos en: </p>
                    <div class="botsocial">
                        <p>
                            <a href="#" onclick="void();" target="_blank"><img src="images/youtube.png" alt="próximamente en Youtube"></a>
                            <a href="#" target="_blank"><img src="images/google-plus.png" alt="próximamente en Google +"></a>
                            <a href="#" target="_blank"><img src="images/facebook.png" alt="próximamente en Facebook"></a>
                        </p>
                    </div>
                  </div>
                </div>
            </div>
    </div>
    <div class="container center-align">
          <div class="col s8" style="margin-top:5px;margin-bottom:5px">
            <p style="font-size:19px;">Copyright &REG; <strong style="font-weight:800;">NIPLE STOCK</strong> &COPY; <?= date("Y");?> Todos los derechos reservados.</p>
          </div>
          <div class="col s4" style="margin-top:5px;margin-bottom:5px">
            <p style="font-size:15px;color: rgba(240, 248, 255, 0.38);">Diseño y Programación <a href="//ahpublic.com" target="_blank">Ah! Publicidad</a></p>
          </div>
    </div>
</footer>
<!--<footer id="codrops-footer">
  <div id="modal1" class="modal bottom-sheet">
    <footer class="page-footer">
      <div class="container">
        <div class="row">
                <div class="col s12 m4 l5">
                    <div class="fwrap">
                        <ul class="row">
                          <div  class="col s6 m6">
                            <h2>NIPLES HIDRAÚLICOS</h2>
                            <li><a href="#hidraulicos"> Hidraulicos</a></li>
                            <li><a href="#bronce"> Bronce</a></li>
                            <li><a href="#aceros"> Aceros</a></li>
                            <li><a href="#plasticos"> Plásticos</a></li>
                          </div>
                          <div  class="col s6 m6">
                            <h2>RESORTES</h2>
                            <li><a href="#balatas"> Balatas</a></li>
                            <li><a href="#alto-tonelaje"> Alto Tonelaje</a></li>
                            <li><a href="#acelerador"> Acelerador</a></li>
                            <li><a href="#caja"> Caja</a></li>
                            <li><a href="#industrial"> Industrial</a></li>
                            <li><a href="#chapas"> Chapas</a></li>
                          </div>
                        </ul>
                    </div>
                </div>
                <div class="col s12 m5 l5">
                    <div class="fwrap">
                        <h3>DIRECCIÓN SANTA CRUZ-BOLIVIA</h3>
                        <div id="datos">
                        <p>Av. Gricotá # 679, 3º Anillo Interno</p>
                        <i style="font-size: 1.2em" class="fa fa-phone"></i>  Telf.: 3507554 <span class="opt"> &nbsp;&nbsp;&nbsp;</span> <i style="font-size: 1.2em" class="fa fa-mobile"></i>Cel.: 72192169 - 78038025</p>
                        <p><i style="font-size: .9em" class="fa fa-envelope"></i> niplestock@hotmail.com  <a href="#mapa" target="_blank">>>Ver mapa <i style="font-size: 1.1em" class="fa fa-map-marker"> </i></a></p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m3 l2">
                    <div class="fwrap">
                        <h1>NIPLE STOCK</h1>
                        <p>Visitenos en: </p>
                        <div class="botsocial">
                            <p>
                                <a href="#" onclick="void();" target="_blank"><img src="images/youtube.png" alt="próximamente en Youtube"></a>
                                <a href="#" target="_blank"><img src="images/google-plus.png" alt="próximamente en Google +"></a>
                                <a href="#" target="_blank"><img src="images/facebook.png" alt="próximamente en Facebook"></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
      </div>
        <div class="container">
          <div class="row">
              <div class="col l6 s12">
                <p>Copyright &REG; NIPLE STOCK &COPY; 2016 TODOS LOS DERECHOS RESERVADOS</p>
              </div>
              <div class="col l4 s12">
                <p>DISEÑO Y PROGRAMACIÓN <a href="//ahpublic.com" target="_blank">Ah! Publicidad</a> - <a href="//gnb.com.bo" target="_blank">GNB</a></p>
              </div>
         </div>
        </div>
    </footer>
  </div>
</footer>
-->
