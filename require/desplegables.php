<?php /*SECCIONES DESPLEGABLES NIPLES*/ ?>
<div id="niples-bronce" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="bronce">Niples de Bronce</h3>
    <?php require('require/secciones/bronce.php'); ?>
  </div>
</div>
<div id="niples-aceros" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="aceros">Niples de Acero</h3>
    <?php require('require/secciones/aceros.php'); ?>
  </div>
</div>
<div id="niples-hidraulicos" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="hidraulicos">Conectores Hidráulicos</h3>
    <?php require('require/secciones/hidraulicos.php'); ?>
  </div>
</div>
<div id="niples-neumaticos" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="neumaticos">Conectores Neumáticos</h3>
    <?php require('require/secciones/neumaticos.php'); ?>
  </div>
</div>

<?php /*SECCIONES DESPLEGABLES RESORTES*/ ?>
<div id="resortes-balatas" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="balatas">Balatas</h3>
    <?php require('require/secciones/balatas.php'); ?>
  </div>
</div>
<div id="resortes-alto-tonelaje" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="alto-tonelaje">Alto Tonelaje</h3>
    <?php require('require/secciones/alto-tonelaje.php'); ?>
  </div>
</div>
<div id="resortes-acelerador" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="acelerador">Acelerador</h3>
    <?php require('require/secciones/acelerador.php'); ?>
  </div>
</div>
<div id="resortes-caja" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="caja">Caja</h3>
    <?php require('require/secciones/caja.php'); ?>
  </div>
</div>
<div id="resortes-industrial" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="industrial">Industrial</h3>
    <?php require('require/secciones/industrial.php'); ?>
  </div>
</div>
<div id="resortes-chapas" class="modal center">
  <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
  <div class="modal-content contenedor">
    <h3 id="chapas">Chapas</h3>
    <?php require('require/secciones/chapas.php'); ?>
  </div>
</div>

<?php /*Muestra de Productos - Balatas*/  ?>
  <div id="balata-10" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/balatas/balata-010.jpg" alt="balatas-niplestock">
    </div>
  </div>
  <div id="balata-11" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/balatas/balata-011.jpg" alt="balatas-niplestock">
    </div>
  </div>
  <div id="balata-12" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/balatas/balata-012.jpg" alt="balatas-niplestock">
    </div>
  </div>
  <div id="balata-15" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/balatas/balata-015.jpg" alt="balatas-niplestock">
    </div>
  </div>
  <div id="balata-19" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/balatas/balata-019.jpg" alt="balatas-niplestock">
    </div>
  </div>

<?php /*Muestra de Productos - Alto tonelaje*/  ?>
  <div id="alto-tonelaje-1001" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/alto-tonelaje/alto-tonelaje-1001.jpg" alt="alto-tonelaje-niplestock">
    </div>
  </div>
  <div id="alto-tonelaje-1002" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/alto-tonelaje/alto-tonelaje-1002.jpg" alt="alto-tonelaje-niplestock">
    </div>
  </div>
  <div id="alto-tonelaje-1003" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/alto-tonelaje/alto-tonelaje-1003.jpg" alt="alto-tonelaje-niplestock">
    </div>
  </div>
  <div id="alto-tonelaje-1011" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/alto-tonelaje/alto-tonelaje-1011.jpg" alt="alto-tonelaje-niplestock">
    </div>
  </div>
  <div id="alto-tonelaje-1017" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/alto-tonelaje/alto-tonelaje-1017.jpg" alt="alto-tonelaje-niplestock">
    </div>
  </div>

<?php /*Muestra de Productos - Acelerador*/  ?>
  <div id="acelerador-001" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/acelerador/acelerador-001.jpg" alt="acelerador-niplestock">
    </div>
  </div>
  <div id="acelerador-002" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/acelerador/acelerador-002.jpg" alt="acelerador-niplestock">
    </div>
  </div>
  <div id="acelerador-004" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/acelerador/acelerador-004.jpg" alt="acelerador-niplestock">
    </div>
  </div>
  <div id="acelerador-008" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/acelerador/acelerador-008.jpg" alt="acelerador-niplestock">
    </div>
  </div>
  <div id="acelerador-010" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/acelerador/acelerador-010.jpg" alt="acelerador-niplestock">
    </div>
  </div>

<?php /*Muestra de Productos - Caja*/  ?>
  <div id="caja-01" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/caja/caja-01.jpg" alt="caja-niplestock">
    </div>
  </div>
  <div id="caja-02" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/caja/caja-02.jpg" alt="caja-niplestock">
    </div>
  </div>
  <div id="caja-03" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/caja/caja-03.jpg" alt="caja-niplestock">
    </div>
  </div>
  <div id="caja-12" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/caja/caja-12.jpg" alt="caja-niplestock">
    </div>
  </div>
  <div id="caja-16" class="modal center">
    <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
    <div class="modal-content center">
      <img src="images/caja/caja-16.jpg" alt="caja-niplestock">
    </div>
  </div>

  <?php /*Muestra de Productos - INDUSTRIAL*/  ?>
    <div id="industrial-01" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/industrial/industrial-01.jpg" alt="industrial-niplestock">
      </div>
    </div>
    <div id="industrial-02" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/industrial/industrial-02.jpg" alt="industrial-niplestock">
      </div>
    </div>
    <div id="industrial-03" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/industrial/industrial-03.jpg" alt="industrial-niplestock">
      </div>
    </div>
    <div id="industrial-04" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/industrial/industrial-04.jpg" alt="industrial-niplestock">
      </div>
    </div>
    <div id="industrial-05" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/industrial/industrial-05.jpg" alt="industrial-niplestock">
      </div>
    </div>

  <?php /*Muestra de Productos - BRONCE*/  ?>
    <div id="bronce-01" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-01.jpg" alt="bronce-niplestock">
      </div>
    </div>
    <div id="bronce-02" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-02.jpg" alt="bronce-niplestock">
      </div>
    </div>
    <div id="bronce-03" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-03.jpg" alt="bronce-niplestock">
      </div>
    </div>
    <div id="bronce-04" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-04.jpg" alt="bronce-niplestock">
      </div>
    </div>
    <div id="bronce-05" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-05.jpg" alt="bronce-niplestock">
      </div>
    </div>
    <div id="bronce-06" class="modal center">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
      <div class="modal-content center">
        <img src="images/bronce/bronce-06.jpg" alt="bronce-niplestock">
      </div>
    </div>

    <?php /*Muestra de Productos - ACEROS*/  ?>
      <div id="acero-01" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-01.jpg" alt="aceros-niplestock">
        </div>
      </div>
      <div id="acero-02" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-02.jpg" alt="aceros-niplestock">
        </div>
      </div>
      <div id="acero-03" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-03.jpg" alt="aceros-niplestock">
        </div>
      </div>
      <div id="acero-04" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-04.jpg" alt="aceros-niplestock">
        </div>
      </div>
      <div id="acero-05" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-05.jpg" alt="aceros-niplestock">
        </div>
      </div>
      <div id="acero-06" class="modal center">
        <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
        <div class="modal-content center">
          <img src="images/acero/acero-06.jpg" alt="aceros-niplestock">
        </div>
      </div>

      <?php /*Muestra de Productos - HIDRAULICOS*/  ?>
        <div id="hidraulico-01" class="modal center">
          <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
          <div class="modal-content center">
            <img src="images/hidraulicos/hidraulico-01.jpg" alt="hidraulicos-niplestock">
          </div>
        </div>
        <div id="hidraulico-02" class="modal center">
          <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
          <div class="modal-content center">
            <img src="images/hidraulicos/hidraulico-02.jpg" alt="hidraulicos-niplestock">
          </div>
        </div>
        <div id="hidraulico-03" class="modal center">
          <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
          <div class="modal-content center">
            <img src="images/hidraulicos/hidraulico-03.jpg" alt="hidraulicos-niplestock">
          </div>
        </div>
        <div id="hidraulico-04" class="modal center">
          <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
          <div class="modal-content center">
            <img src="images/hidraulicos/hidraulico-04.jpg" alt="hidraulicos-niplestock">
          </div>
        </div>
        <div id="hidraulico-05" class="modal center">
          <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
          <div class="modal-content center">
            <img src="images/hidraulicos/hidraulico-05.jpg" alt="hidraulicos-niplestock">
          </div>
        </div>

        <?php /*Muestra de Productos - NEUMATICOS*/  ?>
          <div id="neumatico-01" class="modal center">
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
            <div class="modal-content center">
              <img src="images/neumaticos/neumatico-01.jpg" alt="neumaticos-niplestock">
            </div>
          </div>
          <div id="neumatico-02" class="modal center">
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
            <div class="modal-content center">
              <img src="images/neumaticos/neumatico-02.jpg" alt="neumaticos-niplestock">
            </div>
          </div>
          <div id="neumatico-03" class="modal center">
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
            <div class="modal-content center">
              <img src="images/neumaticos/neumatico-03.jpg" alt="neumaticos-niplestock">
            </div>
          </div>
          <div id="neumatico-04" class="modal center">
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
            <div class="modal-content center">
              <img src="images/neumaticos/neumatico-04.jpg" alt="neumaticos-niplestock">
            </div>
          </div>
          <div id="neumatico-05" class="modal center">
            <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
            <div class="modal-content center">
              <img src="images/neumaticos/neumatico-05.jpg" alt="neumaticos-niplestock">
            </div>
          </div>

          <?php /*Muestra de Productos - CHAPAS*/  ?>
            <div id="chapa-01" class="modal center">
              <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
              <div class="modal-content center">
                <img src="images/chapas/resorte-chapa1.jpg" alt="chapas-niplestock">
              </div>
            </div>
            <div id="chapa-02" class="modal center">
              <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
              <div class="modal-content center">
                <img src="images/chapas/resorte-chapa2.jpg" alt="chapas-niplestock">
              </div>
            </div>
            <div id="chapa-03" class="modal center">
              <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat"><i class="material-icons">clear</i></a>
              <div class="modal-content center">
                <img src="images/chapas/resorte-chapa3.jpg" alt="chapas-niplestock">
              </div>
            </div>
