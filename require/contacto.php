<h2>Contáctenos</h2>
<div class="contenedor">
  <div class="row">
    <form method="post" id="theForm" class="second" action="mail.php" role="form">
            <div class="col s6">
              <div class="form_row">
                <div class="input">
                  <label for="nombre">Nombre completo:</label>
                  <input type="text" id="nombre" name="nombre" tabindex="1" required>
                </div>
              </div>

              <div class="form_row">
                <div class="input">
                  <label for="telefono">Teléfono:</label>
                  <input type="number" id="telefono" name="telefono" tabindex="2" required>
                </div>
              </div>

              <div class="form_row">
                <div class="input">
                  <label for="movil">Teléfono móvil:</label>
                  <input type="number" id="movil" name="movil" tabindex="3" required>
                </div>
              </div>
              <div class="form_row">
                <div class="input">
                  <label for="direccion">Dirección:</label>
                  <input type="text" id="direccion" name="direccion" tabindex="4" required>
                </div>
              </div>

              <div class="form_row">
                <div class="input">
                  <label for="ciudad">Ciudad:</label>
                  <input type="text" id="ciudad" name="ciudad" tabindex="5" required>
                </div>
              </div>
            </div><!-- fin de colf1 -->
            <div class="col s6">
              <div class="form_row">
                <div class="input">
                  <label for="email">Su e-mail:</label>
                  <input type="email" id="email" name="email" tabindex="6" required>
                </div>
              </div>

              <div class="form_row mensaje">
                <div class="input">
                  <label for="mensaje">Mensaje:</label>
                  <textarea id="mensaje" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                </div>
              </div>
              <div class="form_row">
                <div class="g-recaptcha" data-sitekey="6Le2uyQUAAAAAOgmRVZcoxHd-H0usETCd2C1XroM"></div>
              </div>
              <div class="form_row botones">
                <input class="submitbtn" type="submit" tabindex="8" value="Enviar">
                <input class="deletebtn" type="reset" tabindex="9" value="Borrar">
              </div>
            </div><!-- fin de colf2 -->
            <div class="col s12">
              <div id="statusMessage"></div>
            </div>
    </form>
  </div><!-- fin de row -->
  <h3>Ubíquenos</h3>
  <iframe height="480" src="https://www.google.com/maps/d/embed?mid=15zX_909H0FfLn0DXXzr6rcr09uM&hl=es"></iframe>
</div>
