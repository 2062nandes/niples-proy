<h2>Resortes</h2>
<div class="contenedor row">
  <div class="col 12">
      <p>NIPLE STOCK Realiza la fabricación de todo tipo de resortes industriales, de alto tonelaje, balatas, acelerador, caja, chapas etc. Para satisfacer las necesidades de sus clientes, se cuenta con personal de gran experiencia en la fabricación resortes para maquinaria agrícola y en mayor proporción en resortes de frenos para maquinaria pesada y automotores.</p>
  </div>
  <div class="col s6">
  <h3 id="balatas">Balatas</h3>
  <div class="content-carousel">
    <?php require('require/secciones/balatas.php'); ?>
  </div>
  </div>
  <div class="col s6">
  <h3 id="alto-tonelaje">Alto Tonelaje</h3>
<div class="content-carousel">
  <?php require('require/secciones/alto-tonelaje.php'); ?>
</div>
  </div>
  <div class="col s6">
  <h3 id="acelerador">Acelerador</h3>
<div class="content-carousel">
  <?php require('require/secciones/acelerador.php'); ?>
</div>
  </div>
  <div class="col s6">
  <h3 id="caja">Caja</h3>
<div class="content-carousel">
  <?php require('require/secciones/caja.php'); ?>
</div>
  </div>
  <div class="col s6">
  <h3 id="industrial">Industrial</h3>
<div class="content-carousel">
  <?php require('require/secciones/industrial.php'); ?>
</div>
  </div>
  <div class="col s6">
  <h3 id="chapas">Chapas</h3>
  <div class="content-carousel">
    <?php require('require/secciones/chapas.php') ?>
  </div>
  </div>
</div>
