<h2>Nosotros</h2>
<div class="contenedor">
  <h3>Quienes Somos</h3>
  <p>NIPLE STOCK es una empresa boliviana que desarrolla actividades especializadas en la fabricación a medida de todo tipo de resortes, niples y prensados de manguera.  Comprometidos en ofrecer un servicio personalizado brindando las mejores soluciones del mercado.</p>
  <p>Con nuestros productos, abastecemos el mercado de la industria automotriz, agrícola, como así también, a la electromecánica, hidráulica e industria en general. Tenemos más de 10 años de experiencia y conocimiento en el uso de materiales, equipos y fuerza de trabajo, lo cual nos permite desarrollar productos de alta calidad y a  precios muy competitivos ofreciendo al cliente productos que más se acomoden a sus necesidades, con la finalidad de satisfacer sus exceptivas.</p>
  <h3>Misión</h3>
  <p>Somos una  empresa líder en la producción  y comercialización de resortes, niples y prensado de mangueras de la más alta calidad, brindando un excelente servicio de venta con la finalidad de satisfacer la expectativa de nuestros clientes.</p>
  <h3>Visión</h3>
  <p>Ser una empresa líder que se consolide y mantenga la mejor calidad de fabricación y comercialización de resortes, niples y prensado de mangueras en el mercado nacional, implementando procesos de mejora continua con la finalidad de fortalecer un modelo de negocio competitivo, eficiente y responsable.</p>
  <h3>Valores</h3>
  <p>Valores - Responsabilidad - Ética y honestidad - Compromiso con el cliente - Eficiencia</p>
</div>
