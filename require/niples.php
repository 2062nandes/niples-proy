<h2>Niples</h2>
<div class="contenedor row">
  <div class="col 12">
      <p>NIPLE STOCK es una empresa con amplia experiencia y se dedica a la importación de: Niples hidráulicos, de bronce, acero, cañerías, mangueras hidráulicas con malla de acero. Las mangueras hidráulicas de alta y baja presión conectan el quebrantador a la bomba, Acoples Hidráulicos para aire. Se fabrican flexibles hidráulicos de alta,baja y extrema presión al instante.</p>
      <p>Ofrecemos una amplia variedad de mangueras industriales de succión, expulsión, aire, agua, aceite, hidrocarburos, mangueras automotivas, abrazaderas. Mangueras y conexiones hidráulicas.</p>
      <div class="col s6">
        <h3 id="bronce">Bronce</h3>
        <div class="content-carousel">
          <?php require('require/secciones/bronce.php'); ?>
        </div>
      </div>
      <div class="col s6">
        <h3 id="acero">Aceros</h3>
      <div class="content-carousel">
        <?php require('require/secciones/aceros.php'); ?>
       </div>
      </div>
      <div class="col s6">
        <h3 id="hidraulicos">Conectores Hidráulicos</h3>
      <div class="content-carousel">
        <?php require('require/secciones/hidraulicos.php'); ?>
       </div>
      </div>
      <div class="col s6">
        <h3 id="neumaticos">Conectores Neumáticos</h3>
      <div class="content-carousel">
        <?php require('require/secciones/neumaticos.php'); ?>
       </div>
      </div>
  </div>
</div>
