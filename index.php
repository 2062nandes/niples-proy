<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="es"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="es"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="es"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="es"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"><!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>NIPLE STOCK - Prensado de Mangueras Hidráulicas y Fabricación de Resortes</title>
		<link rel="icon" href="favicon.png" type="image/png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
		<meta name="description" content="Niples hidráulicos, bronce, acero y plásticos. Resortes, balatas, alto tonelaje, acelerador, caja, industrial, chapas." />
		<meta name="author" content="J.R.O BOLIVIA" />
		<meta name="keywords" content="HTML,CSS,PHP,JavaScript" />
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css' />
		<link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/materialize.min.css" />
		<link rel="stylesheet" type="text/css" href="css/animate.css"/>
		<link rel="stylesheet" type="text/css" href="css/niple.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/res.css">
		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type="text/javascript">
		/*
			$(window).on("scroll", function() {
				 var scrollHeight = $(document).height();
				 var scrollPosition = $(window).height() + $(window).scrollTop();
				 if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
						 $('.modal').modal();
				 }
			 });
			*/
		/*footer desplegable*/
		$(document).ready(function(){
			// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
			$('.modal').modal();
		});
		$(document).ready(function(){
			$('.slider').slider({
				full_width: false,
				indicators: true,
				interval:4500,
				height: 420,
			});
		});
		$(document).ready(function(){
			$('.carousel').carousel();
		});
		$(document).ready(function(){
				$('.carousel').carousel({dist:0});
				window.setInterval(function(){$('.carousel').carousel('next')},1900)
		 });
		</script>
		<script type="text/javascript" src="js/main.js"></script>
	</head>
	<body>
		<div id="fakebg">
			<p class="animated infinite flash" style="position: fixed;    text-align: center;    font-size: 29px;    display: block;    width: 100%;    top: 37%;    overflow: hidden;">CARGANDO</p>
			<div class="progress"><div class="inner"></div>
				<div class="indeterminate"></div>
			</div>
		</div>
		<!-- Header with Navigation -->
		<div id="header" style="display:none;">
		</div><!--FOOTER DESPLEGABLE-->
		<!-- Inicio -->
		<div id="inicio" class="row">
			<div id="menu-lateral" class="col s3">
				<?php require('require/menu-lateral.php'); ?>
			</div>
			<div class="content">
				<div class="col s10">
					<?php require('require/cabecera.php'); ?>
					<?php require('require/inicio.php'); ?>
				</div><br>
				<?php require('require/footer.php'); ?>
			</div>
		</div>
		<!-- /Inicio -->
		<!-- Nosotros -->
		<div id="nosotros" class="panel row">
			<div id="menu-lateral" class="col s3">
				<?php require('require/menu-lateral.php'); ?>
			</div>
			<div class="content">
				<div class="col s10">
					<?php require('require/cabecera.php'); ?>
					<?php require('require/nosotros.php'); ?>
				</div><br>
				<?php require('require/footer.php'); ?>
			</div>
		</div>
		<!-- /Nosotros -->

		<!-- Contáctenos -->
		<div id="contacto" class="panel row">
			<div id="menu-lateral" class="col s3">
				<?php require('require/menu-lateral.php'); ?>
			</div>
			<div class="content">
				<div class="col s10">
					<?php require('require/cabecera.php'); ?>
					<?php require('require/contacto.php'); ?>
				</div><br>
				<?php require('require/footer.php'); ?>
			</div>
		</div>
		<!-- /Contáctenos -->
		<!-- Niples-->
		<div id="niples" class="panel row">
			<div id="menu-lateral" class="col s3">
					<?php require('require/menu-lateral.php'); ?>
			</div>
			<div class="content">
				<div class="col s10">
					<?php require('require/cabecera.php'); ?>
					<?php require('require/niples.php'); ?>
				</div><br>
				<?php require('require/footer.php'); ?>
			</div>
		</div>
		<!-- /Niples-->
		<!-- Resortes -->
		<div id="resortes" class="panel row">
			<div id="menu-lateral" class="col s3">
				<?php require('require/menu-lateral.php'); ?>
			</div>
			<div class="content">
				<div class="col s10">
					<?php require('require/cabecera.php'); ?>
					<?php require('require/resortes.php'); ?>
			</div><br>
			<?php require('require/footer.php'); ?>
			</div>
		</div>
		<!-- MUESTRAS DEPLEGABLE PRODUCTOS-->
	<section id="muestra-prod">
		<div id="modal2" class="modal">
			<div class="modal-content">
				<a href="#resortes" class=" modal-action modal-close waves-effect waves-green btn-flat">Salir</a>
			</div>
			<div class="modal-footer">
				<h4>Modal Header</h4>
				<p>A bunch of text</p>
			</div>
		</div>
		<?php require('require/desplegables.php'); ?>
	</section>
	</body>
</html>
