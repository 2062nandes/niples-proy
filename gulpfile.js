'use strict';
  const gulp = require('gulp'),
        livereload = require('gulp-livereload'),
        pug = require('gulp-pug'),
        watch = require('gulp-watch'),
        postCss = require('gulp-postcss'),
        simpleVars = require('postcss-simple-vars'),
        nested = require('postcss-nested'),
        colorRgbaFallback = require('postcss-color-rgba-fallback'),
        opacity = require('postcss-opacity'),
        pseudoelements = require('postcss-pseudoelements'),
        vmin = require('postcss-vmin'),
        pixrem = require('pixrem'),
        willChange = require('postcss-will-change'),
        customMedia = require('postcss-custom-media'),
        mediaMinMax = require('postcss-media-minmax'),
        cssnano = require('cssnano');
  let postCssPlugins = [
    simpleVars,
    nested,
    cssnano({
      autoprefixer: {
        add:true
      },
      // core: false
    }),
    colorRgbaFallback,
    opacity,
    pseudoelements,
    vmin,
    pixrem,
    willChange,
    customMedia,
    mediaMinMax
  ];
  gulp.task('styles', ()=>
    gulp.src('./src/*.css')
        .pipe(postCss(postCssPlugins))
        .pipe(gulp.dest('./css'))
  );

  gulp.task('pug', () =>
    gulp.src('./pug/views/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./'))
  );

  gulp.task('default', [], function(){
    gulp.watch('./src/*.css',['styles']);
    // gulp.watch('./pug/views/*.pug',['pug']);
  });
