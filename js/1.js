(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.subiendomano = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA0000").s().p("ABdETIAAABQgSgIgUgOIgggYQgzgdibheQAxgagMggQBtAbBTAfIADABQAuAPACgBIABAAQAHgFADgIQADgJgEgIQgEgJgJgDIkJg+QhChMh8glIAWgOIgBABQAzgmARgzIAAAAQAGgMgJg8QAhgTCoAiQEZA4BvCUQAxBBgFAlQgJAdgYgFQhagHgmgQQgJgDgIAEQgFABgDADQAmAYBKA2QAwAmgHASQgVAYheALIgVgaIAAAAIgBgCIgBAAQgngsgrgYIgcgLQAGALARAYIA4BOIAPASQgrAagaAAQgHAAgGgCg");
	this.shape.setTransform(55.2,39.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ABlFCQgjgYgpglIgJgDIgBAAIgCgBIAAAAIgBAAQgngQgVgSQgVgThQggQhIgdBRgwQAJgegLgKIgBgBQg2gyiZgmQgFAAgFgDIgBgBIgBgBIgKAAIgBAAQg8AEgcgoIAAAAQgfgmAVg5QARgzAzglIAAgBQAzglA1gDQAugCAbAWQA3gaCxAkQEvA8B2CiQBBBXgOA0QgHAsg1AUQA0A2ggAmQgYAnhzAPQgEAUgUAPQggAZglAAQgwAAg5gogAD3EZIgQgSIg4hOQgQgYgGgLIAbALQAsAYAmAsIABAAIABACIABAAIAVAaQBdgLAVgYQAIgSgwgmQhKg2gmgYQADgDAEgBQAJgEAIADQAnAQBZAHQAYAFAJgdQAGglgxhBQhviUkZg4QiogighATQAIA8gGAMIABAAQgSAzgyAmIAAgBIgWAOQB9AlBCBMIEIA+QAJADAEAJQAFAIgDAJQgDAIgIAFIgBAAQgBABgvgPIgCgBQhVgfhsgbQAMAggwAaQCaBeAzAdIAhAYQAUAOASAIIAAgBQAbAJA3ghgAlbhTIAzggIAAAAQAmgdAOgmQACgJgHguQgRgRgeABQgpADgmAcQgnAdgNAmQgMAfAPAVIAAABQAQATAhgCIABAAIAHAAQANAAAHACgAGpllQgFgBgEgDIAuAAIgNAEIgNABQgGAAgFgBg");
	this.shape_1.setTransform(48,36.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,96.1,72.4);


(lib.PupilaNiple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(0.1,1,1).p("AAiAAQABAegHAXQgIAWgNADQgCAAgBAAQgLAAgKgSQgMgUgDgfQgBgKAAgIQAAgWAGgSQAHgaAPgBQANgCAMAYQALAWADAgg");
	this.shape.setTransform(3.4,7.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAEBOQgLAAgKgSQgMgUgDgfIgBgSQAAgWAGgSQAHgaAPgBQANgCAMAYQALAWADAgQABAegHAXQgIAWgNADIgDAAg");
	this.shape_1.setTransform(3.4,7.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.9,17.5);


(lib.pulgarsemi = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AAlAdQACABABABQABAAACABAAfAhIAAAAIAAAAAgpgaIAAABQAAADAAAEQAAACABACQAAgCAAgCQgBgDAAgDIABAAIgBgBQAAAAAAgBQAAgCAAgBIAAgBQAAgBAAgB");
	this.shape.setTransform(10,78.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA0000").s().p("AjtFyQgLgEAOACIAAABIgCAAIADABIACACIgGgCgAjpFyIABAAIACABIgDgBgAjiFtIgKgCQglgNgSgmQgNgcAAgbQAAgaAWgbQAVgbANAIIAWAcQAdAkAXAgQAbAmAHARQgGAKgHAFQgTAQgeAAQgLAAgNgCgAg5FhQgkgNgQgKQgQgLgwhEQgwhEgFgSIAAgFQgCgRACgUQAFhIAohsQAdhogSgzIgdhMQgHgSAQgiQARghAvACQAwADAMAnQAMAnAPAlIARAtQgcAqgCAhQgCAiASANQgdAGgQAOQgYATgDAtQgEAqA3BMQggAjAEAnQADAnAgAiQAgAiAtACIA+ACQgGgbgqAFQg3AAgXggQgXgfAEggQADggAxgWQAwgXAlgBQAkgBARAXQgDgwhPANQhPAMgPAGQgqg0AAgnQgBgmAUgbQAUgbB1ARQAqAHAPAFQASAFAAAEQAIgFAIgJQAHgKgYgEIiXgZQgFABgIgLQgIgMADgVQACgWADgGIAFgJQAGgMAagYQAbgXAxgBQAxAABFAyQBEAyANAUQANAUgDADIgcASQgYAPAIBIQAHBJgSAoQgRAPgFAfQgFAiAAAGQABAHgGAcQgGAcgiAQQgjAQg0gIQgXA4gsABIgHAAQgnAAgigLg");
	this.shape_1.setTransform(37.3,44.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AlZEuIAcgMIACgBIAFgCIAEgBIACgBIAAgBIAMgFQgCgGgCgSQgEgaAHgZQAHgaAvgtQAGhQAihPIAIgTIgCADQAyh7gVg9QgRgzgJgRQgThAAggdQAggcAqgJQAqgKAfBLQARAqAhBLIAbgOQAkgTA9AKQA9AJBVBJQAgAdAOAUIAGALQAGAOgDAIQgBAEgIAGQgHAFgNAFQgaAMANBQQAMBRgnAmQgJAJgCAhQgDAkgHAmQgIAmgjAPQglAQgoAAQgMAkgtAJQgtAIg2gUIhJgaQgHAKgOAIQgGAEgHADQghAQgcgCIgKgBIgTgCIgLAGIAIgFIgIAEIgDACIgtAYIghASIgjATgAjXFfIAAgBIAAgBIAAgBIAAAAgAjSFbQADABABAAQABAAABAAQAAAAAAAAQAAAAgCgBIADABIgCgBIgBAAIgDgCIACAAIAAAAIgHgBQgDAAAHADgAj/C1QgWAaAAAbQAAAaANAcQASAnAlAMIAKACQAuAJAbgWQAHgFAGgLQgHgQgbgnQgXgfgdglIgWgbQgDgCgDAAQgMAAgQAVgAijlrQgQAiAHASIAdBMQASAygdBoQgoBtgFBIQgCAUACARIAAAFQAFARAwBEQAwBEAQALQAQALAkAMQAjAMAtgBQAsgBAXg4QA0AIAjgPQAigQAGgcQAGgdgBgGQAAgHAFghQAFgfARgQQASgngHhIQgIhJAYgPIAcgTQADgDgNgTQgNgUhEgyQhFgzgxABQgxAAgbAYQgbAXgGAMIgFAKQgDAFgCAWQgDAWAIALQAIAMAFgBICYAZQAYAEgHAJQgIAKgIAFQAAgFgSgFQgPgEgqgIQh1gQgUAbQgUAaABAoQAAAmAqAzQAPgFBPgNQBPgMADAvQgRgWgkABQglABgxAWQgwAWgDAgQgEAgAXAgQAWAfA4AAQAqgEAGAbIg+gDQgugCgfgiQggghgDgoQgEgnAggiQg3hMAEgrQADgtAYgTQAQgOAdgGQgSgNACgiQACggAbgrIgQgsQgPglgMgnQgMgogwgCIgHAAQgqAAgPAfgAkgEhIAAABIAAAHIABAEIAAgEIgBgGIABAAIgBgBIAAgBIAAgDIAAgBIAAgCIAAgBIgCAAQgBAEADADg");
	this.shape_2.setTransform(34.6,46.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,4,69.3,85.3);


(lib.pulgarmover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AAlAdQACABABABQABAAACABAgqgaIAAABQABADAAAEQAAACAAACQAAgCAAgCQAAgDAAgDIAAAAIAAgBQAAAAAAgBQAAgCAAgBIAAgBQgBgBAAgBAAeAhIABAAIAAAA");
	this.shape.setTransform(6.4,78.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA0000").s().p("AjtFgQgLgFAOADIAAAAIgCAAIADACIACABIgGgBgAjpFgIABAAIACABIgDgBgAjsFYQglgMgSgnQgNgcAAgaQAAgbAWgaQAVgbANAIQArA1AfAqQAbAnAHAQQgGALgHAFQgTAPgeAAQgPAAgTgEgAg5FOQgkgMgQgLQgQgLgwhEQgwhEgFgRQgIhPA1iQQB4hOg0gIQg0gIABgqIACgwQACgZAjgmQArgwAOAfQAOAgAPAlIARAsQgcArgCAgQgCAiASANQgdAGgQAOQgYATgDAtQgEArA3BMQggAiAEAnQADAoAgAhQAgAiAtACIA+ADQgGgbgqAEQg3AAgXgfQgXggAEggQADggAxgWQAwgWAlgBQAkgBARAWQgDgvhPAMQhPANgPAFQgqgzAAgnQgBgnAUgaQAUgbB1AQQAqAIAPAEQASAFAAAFQAIgFAIgKQAHgJgYgEIiXgZQgFABgIgMQgIgLADgWQACgWADgFIAFgKQAGgMAagXQAbgYAxAAQAxgBBFAzQBEAyANAUQANATgDADIgcATQgYAPAIBIQAHBJgSAnQgRAQgFAfQgFAhAAAHQABAGgGAdQgGAcgiAQQgjAPg0gIQgXA4gsABIgHAAQgnAAgigLg");
	this.shape_1.setTransform(33.8,46.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AlHEjIADgBIACgBIABgBIALgGQgCgFgCgSQgDgaAHgZQAGgaAvguQAHhZAphYIgBADQA1h7gLg8QgKg9ArgqQArgqAPgMQAWAGAHARQARAqAhBKIAbgNQAlgTA9AKQA8AIBVBKQAhAdANATIAHAMQAFAOgCAIQgCAEgIAFQgHAFgMAGQgaAMAMBQQAMBQgmAnQgJAIgCAiQgDAkgIAmQgHAlgkAQQgkAPgoAAQgMAlgtAIQguAJg2gUIhIgaQgIAKgNAIQgGAEgIADQghAQgbgCIgegDIgLAGIAIgFIgIAEIgDACIgTAKgAjoFjIAAgBIAAgCIAAAAIgBAAgAjjFfQACAAABABQACAAAAAAQABAAgBAAQAAgBgBAAIACABIgCgBIAAAAIgDgCIABAAIAAAAIgHgBQgDAAAIADgAkRC4QgVAbAAAaQgBAbAOAcQARAnAmAMQA1AMAegXQAHgGAFgKQgGgRgcgmQgegrgsg1QgDgBgDAAQgLAAgRAUgAh0lEQgjAmgCAZIgCAwQgBAqA0AIQA0AIh4BOQg1CQAJBPQAFARAwBEQAwBEAQALQAQALAkAMQAkAMArgBQAtgBAXg4QAzAIAjgPQAjgQAGgcQAFgdAAgHQgBgGAGghQAEggASgPQARgogHhJQgHhHAXgQIAcgSQAEgDgNgTQgOgUhEgyQhEgzgxAAQgyABgaAYQgbAXgGAMIgEAKQgDAFgDAWQgDAVAJAMQAIAMAEgBICYAZQAXADgHAKQgHAJgJAGQABgFgTgFQgPgFgpgHQh2gQgTAbQgUAaAAAnQAAAnArAzQAOgGBPgMQBQgMADAvQgSgWgkAAQgkACgxAWQgwAWgEAgQgDAgAXAfQAWAgA3AAQArgEAGAaIg/gCQgtgCgggiQggghgDgoQgDgnAggiQg4hNAEgqQAEgtAYgTQAQgOAdgGQgSgNACgiQABggAdgrIgSgtQgOglgOgfQgGgMgKAAQgQAAgaAdgAkyElIAAAAIABAIIAAAEIAAgEIAAgGIAAAAIAAgBIAAgBIAAgDIAAgBIgBgCIAAgBIgBAAQgBADACAEg");
	this.shape_2.setTransform(32.8,46.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,8.3,65.7,75.8);


(lib.pulgar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AgqgaIAAABQABADAAAEQAAACABACQAAgCAAgCQgBgDAAgDIABAAIgBgBQAAAAAAgBQAAgCAAgBIAAgBQgBgBAAgBAAlAdQACABABABQABAAACABAAeAhIABAAIAAAA");
	this.shape.setTransform(105.6,78.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA0000").s().p("AjtGMQgLgEAOACIAAABIgCAAQAGADgBAAIgGgCgAjpGMIABAAIACABIgDgBgAjiGHIgKgCQglgNgSgmQgNgcAAgbQAAgaAWgbQAVgbANAIIAWAcQAdAlAXAfQAbAmAHARQgGAKgHAFQgTAQgeAAQgLAAgNgCgAg5F7QgkgNgQgKQgQgLgwhEQgwhEgFgSIAAgFQgBgRABgUQAFhHAohtQAdhogSgzQgTgzgVgjQgMgTgEgVQgDgQABgRIACgKQAGgeAXgJQAbgLAtAiQAsAiAOAlIAcBJIARAtQgcAqgCAhQgCAiASANQgdAGgQAOQgYATgDAsQgEArA3BMQggAjAEAnQADAnAgAiQAgAiAtACIA+ACQgGgbgqAFQg3AAgXggQgXgfAEggQADggAxgWQAwgXAlgBQAkgBARAXQgDgwhPANQhPAMgPAGQgqg0AAgnQgBgnAUgaQAUgbB1ARQAqAHAPAFQASAFAAAEQAIgFAIgJQAHgKgYgEIiXgZQgFABgIgLQgIgMADgVQACgWADgGIAFgJQAGgMAagYQAbgXAxgBQAxAABFAyQBEAyANAUQANAUgDADIgcASQgYAPAIBIQAHBJgSAoQgRAPgFAfQgFAiAAAGQABAHgGAcQgGAcgiAQQgjAQg0gIQgXA4gsABIgHAAQgnAAgigLg");
	this.shape_1.setTransform(132.9,41.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AshEWQDwgKCogLQE2gTBFgTIAcgIIA9gSIAKgEIABAAIAIgCIAmgNIAcgLIACgBIAFgCIAEgCIACgBIAAAAIAMgGQgCgGgCgSQgEgZAHgZQAHgaAvguQAGhPAihQIAIgSIgCADQAyh7gVg+QgTg5gQgeIgDgEQgRgegEgXQgDgWALgjQAGgQAKgJQAagYA5AXQA3AUAfBLQARAqAiBKIAbgOQAkgSA9AJQA9AJBVBKQAgAcAOAUIAGAMQAGANgDAJQgBAEgIAFQgHAFgNAGQgaAMANBRQAMBQgnAmQgJAIgCAiQgDAjgHAmQgIAmgjAQQglAPgoAAQgMAkgtAJQgtAJg3gVIhJgaQgHAKgOAJQgGAEgHADQghAQgcgCIgKgBIgTgCIgLAFIAIgEIgIAEIgDACIgtAYIghASIgjATIgmATIgIAEIhIAiQi5BUjeBBQiwAyjHAmQhag6A5kBgAEGDgIAAgBIAAgCIAAAAIAAAAgAELDbQALAEgKgFIADABIADABIgCgBIgBAAIgDgBIACAAIAAgBIgHgBQgDAAAHADgADeA1QgWAbAAAaQAAAbANAcQASAmAlANIAKACQAuAIAbgWQAHgFAGgKQgHgRgbgmQgXgggdglIgWgbQgDgCgDAAQgMAAgQAVgAEzo7QgXAJgGAdIgCALQgCARAEAQQAEAUAMAUQAVAjATAzQASAzgdBoQgoBtgFBIQgCAUACAQIAAAFQAFASAwBEQAwBEAQALQAQAKAkANQAkAMAtgBQAsgBAXg4QA0AIAjgQQAigQAGgcQAGgcgBgHQAAgGAFgiQAFgeARgPQASgogHhJQgIhJAYgPIAcgSQADgDgNgUQgNgUhEgyQhFgygxAAQgxABgbAXQgbAYgGAMIgFAJQgDAGgCAWQgDAVAIAMQAIALAFgBICYAZQAYAEgHAKQgIAJgIAFQAAgEgSgFQgPgFgqgHQh2gRgUAbQgUAbABAnQAAAnAqA0QAPgGBQgMQBPgNADAwQgRgXgkABQglABgxAXQgxAVgDAgQgEAgAXAfQAXAgA4AAQAqgFAGAbIg+gCQgugCgggiQgggigDgnQgEgnAggiQg3hMAEgrQADgtAYgTQAQgOAdgGQgSgNACgiQACghAcgqIgRgtIgchJQgOglgsgiQgigagXAAQgIAAgHADgAC9CiIAAAAIAAAIIABAEIAAgEIgBgHIABAAIgBAAIAAgBIAAgEIAAAAIAAgDIAAAAIgCAAQgBADADAEgAEMDaIAAAAg");
	this.shape_2.setTransform(82.4,59.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,164.9,118.7);


(lib.piernaniple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AidC9ICVlqQCYhoAOCmIjVE7QgkAKgYAAQgkAAgGgZg");
	this.shape.setTransform(15.8,21.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,31.6,42.8);


(lib.pie = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA0000").s().p("Ai2EFQAMAAh4gTIg1gIQivgbAfg8QAfg8Cfg4QCfg4AcAUQBGBZAZgGQAYgHgfgfQghgfgSgLQgTgNgEgGQAJACA5gVIAkAoQAhAkAUAPQAbAAgBgMQgBgMhjhIQAUgaAeATQAeASApAeQAqAcgFgZQgFgbgvgXIgwgXQBmg+ATgbQCQAbBmAKIAUAeQASAcAcA4QAcA4gTBBQgUBAiHgBQiGgBhwA4QgRAIgTAHQhCAZhRAAQgoAAgrgGgAFmiZQgogEhCgQQhCgRAAgEQgMgRACgPQABgOANgMQBygXBnAQQATAPAFASQACAKgCALQgGAggWAZIgtgFg");
	this.shape.setTransform(52.4,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AjSEdIiwgkQgbgGgVgHQhLgYgGgoQgLhDCbhJQCYhHBmACQAAAAABAAQAAAAABAAQAAABABAAQAAAAAAABIAIAGIgDgDQgGgHAGgCIAAAAQADgDBMgUIADAAIACACIACABQAAAAAAgBQAAAAAAAAQABAAAAgBQAAAAAAAAQASgQAXgCIAxghQApgdAhgcQAUgRAFgHQgIgTADgSQAEgVAJgMQAIgNBSgJQBSgJBIAPQAfATAFAeQAFAcgUAmIAAABIgBABIgBACIgRATIABABIACADIABAAIABABIAUAfQASAcAeA6QAdA5gJBEQgKBEg+APQg+AOhaACQhaABgtAgQgtAghxAVQg5ADgrAAQhJAAgigHgAkhAZQifA4gfA8QgfA8CvAbIA0AIQB4ATgMAAQCDASBkglQASgHASgIQBvg4CHABQCHABAThAQAUhBgcg4Qgdg4gRgcIgUgeIACADIgBgCIgBgBQhngKiQgbQgSAbhnA+IAxAXQAvAXAFAbQAEAZgpgcQgqgegdgSQgegTgVAaQBjBIABAMQABAMgbAAQgTgPghgjIgkgpQg5AVgKgCQAFAGASANQATALAgAfQAgAfgZAHQgZAGhGhZQgHgFgQAAQguAAh1ApgADFkCQgMAMgCAOQgCAPAMARQAAAEBDARQBCAQAoAEIAsAFQAXgZAGggQACgLgDgKQgFgSgTgPQgqgHgsAAQg/AAhEAOg");
	this.shape_1.setTransform(51.7,29.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,103.3,58.3);


(lib.NipleOjo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AAGi3QALARANAyQAQA/gKBEQgJBFgfA4QgSAfgRANQBrilg+jKg");
	this.shape.setTransform(15.8,21.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgLDBQgwgKgPg+QgPg9AJhGQAJhHAcgpQAcgoAcgTQAcgSAOALIAGAGQA+DJhrClQgMAJgLAAIgEAAg");
	this.shape_1.setTransform(10.3,21.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AheCKQgThIAHhHQAHhJAeg3IAAABQAfg5AvgTQAbgLAVASIABABQATAQAOAqQAaBRgLBNQgNBNgeA7QgWAsgiAMQgLAFgNAAQg5gDgUhIgAAHiwQgcASgcApQgcAogJBHQgJBGAPA+QAPA9AwAKQANACANgLQARgMASgfQAgg4AJhFQAKhEgQg/QgNgygLgRIgGgGQgFgEgHAAQgNAAgRAMg");
	this.shape_2.setTransform(10.9,21.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,21.9,42.6);


(lib.guineo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAlBGQhxgQhXAOQC5gyCWA2IADADQAAAAAAAAQAAABAAAAQgBABAAAAQAAABgBABQgCACgJACIgFAAQgVABhjgOgADaBAQgQgBAEgBQAEgBi6g1Qi6g0gSAIIgVAJQgDAWgIgCQgJgDgEgNQgFgNADgGQADgFAqgJQApgIFPBpQgdgvg2ggQg2ggAAgCQgBAAAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIAGgHQAFgEAqAaQApAbAiAnIApAyQAHAIgMAAIgEAAg");
	this.shape.setTransform(22.9,8.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,45.8,16.7);


(lib.cuerponiple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AmkgCQAehKDLgKQDLgLDTADQDSADgKA/QgKA9htAcQmngujkBQQhsgXAfhKg");
	this.shape.setTransform(66.7,176.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3FFFF").s().p("AgmUzQgdgHhGhMQhGhLggg/Qghg/AGgJQAGgIANABIABAAQHPA+BghQQAxgGg1B4IgVAxIgdAvQg1BZgoARQhiAFg1AAQgoAAgNgDgAjnP3QhNh6gEhEQgEhEA9gUQE4AqEgg9QBNCYiNCHQg2AgiTAAQh6AAi9gWgAkGLMQhUhMAwhPQDphYG5BAQAzBshcAzQjAAfi7AAQhtAAhtgLgAmEBeIADgZIACgJQAJgfAogpILhAAQAnBNhFETQoegQiDApQhhipAJhmgAHflLIgCAAIgDgBIAAAAIgxgMIiFgPQgfADgGARIgBAGQgBgSgegKIgKgDIAAgBQh6gKhxAAQhyAAgeAFQgeAEgWAFIgyALQgFgKgDgBQgHgDgKgBIhkAJIgOACIg2ALIgGgJQgUgZgWggQgWggAJgeQAJgfAbgOIA+ghQAFAAgjgDQg5ABgIgdQgIgcAkgZQAkgZAogPQAVgEg6gEQg7gFgPgSQgQgSAwg0QAvgzARgIIASgKQAAgBhOgOQhPgNBbhGIBCg0QgNABg0gJQg0gKgDgUQgDgTAegSIA4ggQg1gUgUgQQgVgQAggsQAggsBIgWQBIgXCCgbQCBgbCQAHQCCAKC8AfQA3AXATAiQARAhgZALQgaAKABABIAxAwQAxAwgjAYQgjAZgKgDIAWAYQAZAXAKAlQAJAkgSAPQgSAOgGgFQArAtAIAxQAHAxgeAGQgeAFgBACQAAAAgBAAQAAABAAAAQAAABgBAAQAAABAAAAQAAABAAAAQABABAAAAQAAAAAAABQABAAAAABQA9A/AGAjQACAPgKAFQgKAFgNAAIgZACIARAbQApBAgEAkQgEAjgqACIgWABQANAUAIAJQAJALADAkQACAggPACIgEgBg");
	this.shape_1.setTransform(61.9,132);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("ApMCWQgIgPgHgZQgRg6AIhnIADgVQAEgRBigcQBjgbAngEIAwgEIALAAIALgBQAFAGACAfIgGCuIgCAWQhgBEhbAKQhHAHgLACQgKgCgJgPgAFTBkQg/gfgNgCIgCgBIALizQACgpAFAEQEXAiAjAYQAiAXgRDKIgMABIgjABgAh/BeQhygNgCgKIgIgnQgGgcABgkQABhUADghQAIgIChgHQChgJCDAXQAcBegiCFQiXAVhkAAQgsAAgjgEg");
	this.shape_2.setTransform(62.8,114.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah/CjIAAgCIAAADgAB/ijIABAAIAAAAIgCABg");
	this.shape_3.setTransform(19.9,115);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AiYUOQh7ifgghIQgRgnAIgPQAGgPAMgDQguh0gGhHQgGhJAvgWQhBhYAThIQhGglAHg9QAGg+A4gYQhSidAViSIABgCQithAgMgSQgnhMABhNQABhRACgaQAPgZAqgaQAYgPAWgDIAJgBIACAAIABAAIADgBQADgLgSghQgdg3ALgpQAKgpAfgJQgwgKgFgmQgEgmA/gnQhBgQAAgpQgBgqBXhNQhegPAFgnQADglBPgtQhMgagGgdQgIgiA5geQg6geAOgwQAMgxA1gkQA1gkDPgqQDOgqDBAaIDDAaQBPAiAeA4QAeA3g2ACQA5AoALA1QAKA0g7ACQAuAlAGAyQAFAxgoABQA0BGgGAwQgFAwgiABQAoArAFAhQAGAhgMANQgLANgQAAQAmBKgDApQgCAqgsAGQAFAMAGAYIACAQQADATgCAVIACAAQAfAEAYAHQBIAXAIA2IAAAAQAOCJgLBLIgBABQgOAXgyAYIhbAtQgqAUgIABIgIAAIgCBZQgDBbgEAhQgEAigFAKQBeAigHAwQgFAthoA3QAnB0hFBAQBbB8h0CWQAIACAHAFQAMAGABATQACAhgxBxQgyBxgqAuQgqAugXAIQgYAIh0ADIgIAAQhsAAguhJgAkcP7QgGAIAhA/QAgA/BGBLQBGBMAdAHQAdAICvgKQAogQA1haIAdguIAVgxQA1h5gxAGQhgBQnPg+IgBAAIgDAAQgLAAgFAIgAlKMlQAEBEBNB6QGcAyBkg8QCNiHhNiYQkgA9k4gqQg9AUAEBEgAk8IdQgwBPBUBMQElAdEwgxQBcgzgzhsQi7gbiVAAQjLAAiHAzgAidFUQjLAKgeBKQgfBLBsAXQDkhQGnAuQBtgcAKg+QAKg/jSgDIheAAQijAAidAIgAmRAoIgCAJIgDAZQgJBmBhCpQCDgpIeAQQBFkTgnhNIrhAAQgoApgJAfgAo2gaIAAABIAAgDgAl6lbQgoAEhiAbQhjAcgDARIgEAVQgIBoASA6QAHAZAIAPQAJAPAJACQALgCBIgHQBbgKBghEIABgWIAGivQgCgfgFgGIgLABIgKAAIgwAEgAEGk4IgLC0IADABQANACA+AfIDgAjIAkgBIALgBQASjLgjgXQgigYkXgiIgBAAQgFAAgCAlgAhclrQiiAHgHAIQgEAhgBBUQgBAlAHAcIAIAnQABAKBzANQByANDYgeQAiiGgcheQhggQhvAAQgqAAgrACgAGXlsIAxAMIAAAAIADABIACAAIAEABQAPgCgCggQgDgkgJgLQgIgJgNgUIAWgBQAqgCAEgjQAEgkgphAIgRgbIAZgCQANAAAKgFQAKgFgCgPQgGgjg9g/QgBgBAAAAQAAAAAAgBQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAAAABgBQAAAAAAAAQAAgBABAAQABgCAegFQAegGgHgxQgIgxgrgtQAGAFASgOQASgPgJgkQgKglgZgXIgWgYQAKADAjgZQAjgYgxgwIgxgwQgBgBAagKQAZgLgRghQgTgig3gXQi8gfiCgKQiQgHiBAbQiCAbhIAXQhIAWggAsQggAsAVAQQAUAQA1AUIg4AgQgeASADATQADAUA0AKQA0AJANgBIhCA0QhbBGBPANQBOAOAAABIgSAKQgRAIgvAzQgwA0AQASQAPASA7AFQA6AEgVAEQgoAPgkAZQgkAZAIAcQAIAdA5gBQAjADgFAAIg+AhQgbAOgJAfQgJAeAWAgQAWAgAUAZIAGAJIA2gLIAOgCIBkgJQAKABAHADQADABAFAKIAygLQAWgFAegEQAegFByAAQBxAAB6AKIAAABIAKADQAeAKABASIABgGQAGgRAfgDgAk3lhIAAABIACgBIgBAAIgBAAgAk3llIACAAIgCAAg");
	this.shape_4.setTransform(63.7,134);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.2,-2.7,128,273.4);


(lib.cejaniple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgzAeQgFgeALgXQALgXAagBQAYgBATAdQASAcABAIQAAAHgHgLQgIgLgJgIQgKgKgHgCQgHgCgMACQgNABgIAHQgIAGgDAKQgDAKgDAWQgBAKgBAAQgCAAgDgSg");
	this.shape.setTransform(5.4,4.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10.8,9.6);


(lib.casicerrado = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABFApIgogDQAJgEAHgVQAJgbgRgLQgLgHgMgDQBmAOBhA0QhBALgxAAIgegBgAjUgUQBogcBkAKQgPABgMAKQgRAMAFATQAFATAQAIIAIAFQh3gRhLgng");
	this.shape.setTransform(30.8,14.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Aj/AVIgDgCIgBgDIABgDQAAgBABAAQAAAAAAAAQAAAAABgBQAAAAABAAQEphbDxChIACACIAAADIgBADIgDABQhEAKhFAAQjDAAjMhPgAA8ASQARALgJAcQgHAVgJAEIAoADQA5AEBXgOQhhg1hmgOQAMADALAHgAi9AXQBLAoB2ARIgHgFQgQgIgFgTQgFgUARgMQAMgKAOgBQgZgCgbAAQhKAAhNAUgACOgPQiAgsgvAAQgvAAiQAMQgOAFgbADQANgGAFgLQgeAKgHgKQAxgVCFgHQCEgIDOBcQgUgfhBgaQhDgbADgFIACgGQABgBAAAAQAAgBAAAAQABAAAAgBQABAAAAAAQABAAAAgBQAAAAABAAQAAAAABAAQAAABABAAQCWA8AbA7IABAEIgEADIAAAAQgEAAh8grg");
	this.shape_1.setTransform(28.5,10);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,57,20);


(lib.canillaniple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AiWCwQgEgkAlgrQAthAAKgSQAlg9A/iOIACgCQAmggAcACQAcABARAlIAAACIAAADQhbCGgWA6QgMAjAKAIIAfASQAbAtg3BGQgCgBguAXQgRAIgUAAQgtAAg7gtg");
	this.shape.setTransform(15.2,22.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,30.3,44.3);


(lib.brazoniple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AmMJVQgkgbgXhLQhTgjAPhmQhIgsgCg6QgEg5BFhGIAAAAQBEg5A8goQA9gnAegWQAegVANgIIAegQQAAgZAIgQIADgGQARgZAcAAQAZgBA2AXIABABIAcgdIAngoQELkbgFiCQEegTBYCZQmNDphKAuIg/AoIgIAFQgcATgSAQQgjAiAMAXIABABIAGAIIACADIgCgCIACADIAHAPQALAUgKAcIgEALQgOAlgxAMIgPADQAsBWgRBPQgLA0gVATQAsArAPAnQAPAogMAOQgnAshihAQhjg/gVANQgWAOAMBMQALBMgpAkQgYAUgXAAQgQAAgQgLgAmQFVQgWA5gDA7QgCA7ArAlQArAmAOg0QAOg0gGguQgGguAVgbQAUgaAaAJQARAFArAVQArAVAQAKQBLArAMgHQALgHgQgiQgRgig1gqQBjhShLijQgBABgngQQgmgRgagfQgZgegHgiQg4AcioB7QipB9BxBqQAehSAtgiQALgIAJAHQAJAIgbAaIgLAMQggAngOBEQgOBEA5AcQAIiKAggPQAIgDAEAAQAPAAgLAcgAi+h8QgZAIgEAqQAKBTBRAoQBQAnAbg9QAKgVgBgRQgBgSgYgTQgcARgcgWQgTgQgLgjIAIgIQgjgPgXAAQgKAAgHADg");
	this.shape.setTransform(59.3,57.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA0000").s().p("AhfFNQgrgmADg7QACg7AWg5QAOglgeAMQggAPgICKQg4gbANhEQAOhEAhgnIAKgMQAbgbgJgHQgJgGgLAHQgtAhgeBTQhxhqCph8QCph9A3gbQAGAhAaAgQAZAfAnAQQAmAQABAAQBLChhjBTQA2ApAQAiQAQAigLAHQgMAHhLgqQgQgKgrgVQgrgVgQgGQgagIgUAaQgVAaAGAuQAHAugPA0QgIAegSAAQgNAAgSgPgACgirQhRgngKhVQAEgpAZgJQAZgIAyAUIgIAJQALAjAUAPQAbAWAcgQQAYASABASQABATgKAUQgQAmglAAQgXAAgfgQg");
	this.shape_1.setTransform(30.4,79.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-3.3,119.7,121.7);


(lib.brazomover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AAFgDIAAAHAgEgDIAAAA");
	this.shape.setTransform(108.1,46.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA0000").s().p("AhLBUQgPgSgEgRQgEgSAUgYQAfAIAWgcQAPgTADglIgLgGQAsgiAbACQAbACAPAnQALBThGA7QgkAggeAAQgaAAgTgYg");
	this.shape_1.setTransform(116.5,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AGnFDIgHgLQgQgZAFgWIAEgRIABgDIABgBQABgIACgBIABgBQAEgSgRgOQgSgOgQgFQgQgFgogSIgJgEIgvgVIg9gWIgdgKQhFgXk1gXQiogNjwgNQg5k5BahGQDHAuCwA+QDdBOC6BnQAlAUAjAVIAIAGIAxAeQAdASAjAaIA9AvIAJAHIAgAdQAvgkAagGQAbgHAXAUIAFAFQAKALAHANQAGAMACAnQAAAdgPAhQgRAlgWAWQgSARgqAQIgOAFIAHgDIgMAEIgLABIgBAAQgyAAgZgegAGqEUQAEARAPARQArA1BGg8QBGg8gMhUQgPgngbgCQgbgBgtAhIALAGQgCAlgQAUQgVAdgggJQgUAZAEASgAG7B2IAAgHgAGyBvIAAAAIgBAAIAAAAIABAAg");
	this.shape_2.setTransform(64.3,35.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,128.6,70.6);


(lib.bocaniple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA0000").s().p("AifATQgOh/CTAqIAAAAQgWBiAXAyQgGgOAAgZQABg3AMg2QBShfBhCmQgzBQhyAQQgLgBgGgKIAHALQhQgKhBhIg");
	this.shape.setTransform(41,43.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AkRArIgUgyIgTguQgCgMAegJQACgBC+AiQC8AiDDhCQAOgHAFAIQAFAJgCAZIgCBLQiiAoiXAAQiMAAiDgig");
	this.shape_1.setTransform(35.4,13.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAXERQhbgIhZhmIgUgYQhBhSg+iCIgGgMIgYg0IAAgBQgrhIABgbQABgfAegEQAcgEAXAtQETA8B3gVQB5gVAxgOQAxgPAMgFIAWgKQAKgFAJAMQAKALgJASQgJASgMAKQgNAKgcAHQgDAsgGAoIgCATQgVCCgxBSQgJAPgKANQhGBch1AOIgBAAgAATEGIgIgLQAHAKALABQBygQAzhQQhhimhTBeQgMA2AAA4QgBAZAGAOQgWgyAVhjIAAAAQiSgqAPCAQBABIBQAKIAAAAgAkzjSQgfAIADANIATAuIAUAzQESBGE2hMIAChMQACgZgFgJQgFgIgOAHQjDBBi8ghQi5ghgHAAIAAAAgAlujtIABAAIgBAAgAlYkFIAAgBIAAAAg");
	this.shape_2.setTransform(37.7,27.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,75.4,54.6);


// stage content:
(lib._1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// extras
	this.instance = new lib.pulgarsemi("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-179.9,31,1,1,0,0,0,34.6,44.6);

	this.instance_1 = new lib.pulgarmover("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(23.2,477,1,1,0,0,0,32.9,42.1);

	this.instance_2 = new lib.brazomover("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-128.1,346,1,1,0,0,0,64.3,35.3);

	this.instance_3 = new lib.subiendomano("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(199.8,443.8,1,1,0,0,0,48.1,36.2);

	this.instance_4 = new lib.casicerrado("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(-123.2,175.6,1,1,0,0,0,28.5,10);

	this.instance_5 = new lib.guineo("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-215.3,151.1,1,1,0,0,0,22.9,8.3);

	this.instance_6 = new lib.pulgar("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(-145.3,124.4,0.861,0.84,0,0,0,7.5,102.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(70));

	// niple
	this.instance_7 = new lib.pie("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(234.4,338.9,1,1,0,0,0,82.7,15.1);

	this.instance_8 = new lib.pie("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(323.7,338.1,1,1,0,-5.4,174.6,82.7,13.4);

	this.instance_9 = new lib.canillaniple("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(326,333.4,1,1,0,0,180,10.5,33.4);

	this.instance_10 = new lib.canillaniple("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(236.8,320.6,1,1,0,0,0,15.2,22.1);

	this.instance_11 = new lib.bocaniple("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(275.3,95.4,1,1,0,0,0,37.7,27.3);

	this.instance_12 = new lib.cejaniple("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(290.1,13.1,1.104,1,0,0,180,5.4,4.8);

	this.instance_13 = new lib.cejaniple("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(264.8,14.7,1,1,0,0,0,5.4,4.8);

	this.instance_14 = new lib.PupilaNiple("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(290,47.1,1.055,0.993,0,-8.8,172.2,3.1,8.2);

	this.instance_15 = new lib.NipleOjo("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(290.7,46.1,1.056,0.991,0,0,180,10.5,21.6);

	this.instance_16 = new lib.PupilaNiple("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(263.8,46.7,1,1,8.3,0,0,3.4,7.8);

	this.instance_17 = new lib.NipleOjo("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(263.1,45.8,1,1,0,0,0,10.9,21.3);

	this.instance_18 = new lib.cuerponiple("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(279.2,292.2,1,1,0,0,0,67.8,264.4);

	this.instance_19 = new lib.piernaniple("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(290.6,272.3,1,1,0,-5,175,24.4,6.5);

	this.instance_20 = new lib.piernaniple("synched",0);
	this.instance_20.parent = this;
	this.instance_20.setTransform(264.2,271.3,0.986,0.986,0.5,0,0,24.6,6.6);

	this.instance_21 = new lib.brazoniple("synched",0);
	this.instance_21.parent = this;
	this.instance_21.setTransform(313.3,174.7,1,1,0,0,180,90.7,12.3);

	this.instance_22 = new lib.brazoniple("synched",0);
	this.instance_22.parent = this;
	this.instance_22.setTransform(240.2,178.2,1,1,0,0,0,90.7,12.3);

	this.instance_23 = new lib.casicerrado("synched",0);
	this.instance_23.parent = this;
	this.instance_23.setTransform(259.4,47.3,0.457,1.323,-20,0,0,29.2,10.5);

	this.instance_24 = new lib.subiendomano("synched",0);
	this.instance_24.parent = this;
	this.instance_24.setTransform(362.9,179.3,0.512,0.701,-15,0,0,16.4,19.6);

	this.instance_25 = new lib.brazomover("synched",0);
	this.instance_25.parent = this;
	this.instance_25.setTransform(336.4,178.9,0.404,0.584,0,-146,34,64.5,35.5);

	this.instance_26 = new lib.pulgarmover("synched",0);
	this.instance_26.parent = this;
	this.instance_26.setTransform(417.3,174.1,1.093,0.558,18.2,0,0,33,42.1);

	this.instance_27 = new lib.guineo("synched",0);
	this.instance_27.parent = this;
	this.instance_27.setTransform(258.9,46.4,0.594,1,0,0,0,23,8.3);

	this.instance_28 = new lib.pulgarsemi("synched",0);
	this.instance_28.parent = this;
	this.instance_28.setTransform(403.1,146.7,0.861,0.8,9.9,0,0,34.6,44.7);

	this.instance_29 = new lib.pulgar("synched",0);
	this.instance_29.parent = this;
	this.instance_29.setTransform(300.9,177.6,0.861,0.84,0,0,0,7.5,102.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_22,p:{rotation:0,x:240.2,y:178.2}},{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:180,x:313.3,y:174.7,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.5,skewX:-5,skewY:175,y:272.3,x:290.6,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:0,x:279.2,y:292.2,regX:67.8}},{t:this.instance_17,p:{regX:10.9,regY:21.3,rotation:0,x:263.1,y:45.8,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.4,rotation:8.3,x:263.8,y:46.7,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:0,skewY:180,x:290.7,y:46.1,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-8.8,skewY:172.2,x:290,y:47.1,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:0,x:264.8,y:14.7,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:0,skewY:180,x:290.1,y:13.1,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.7,regY:27.3,rotation:0,x:275.3,y:95.4}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:0,skewY:180,regX:10.5,regY:33.4,x:326,y:333.4}},{t:this.instance_8},{t:this.instance_7}]}).to({state:[{t:this.instance_22,p:{rotation:3.4,x:246.4,y:176.3}},{t:this.instance_21,p:{regX:90.6,skewX:3.4,skewY:-176.6,x:319.7,y:177.1,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-8,skewY:172,y:272.4,x:290.6,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:3.4,x:278.6,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:3.4,x:277.3,y:45.5,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:11.7,x:277.9,y:46.4,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:3.4,skewY:-176.6,x:304.8,y:47.5,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-5.4,skewY:175.6,x:304,y:48.3,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:3.4,x:280.8,y:14.5,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:3.4,skewY:-176.6,x:306.1,y:14.4,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:3.4,x:286.5,y:95.8}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:3.7,skewY:-176.3,regX:10.5,regY:33.4,x:326,y:333.4}},{t:this.instance_8},{t:this.instance_7}]},11).to({state:[{t:this.instance_22,p:{rotation:5.5,x:250.6,y:175.2}},{t:this.instance_21,p:{regX:90.6,skewX:5.5,skewY:-174.5,x:323.8,y:178.6,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-12.5,skewY:167.5,y:272.4,x:290.7,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:5.5,x:278.6,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.3,rotation:5.5,x:286.1,y:45.5,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.4,rotation:13.7,x:286.6,y:46.6,regY:7.9,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:5.4,skewY:-174.6,x:313.4,y:48.6,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-3.4,skewY:177.7,x:312.7,y:49.4,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:5.5,x:290.7,y:14.8,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:5.5,skewY:-174.5,x:315.9,y:15.6,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:5.5,x:293.5,y:96.2}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:11,skewY:-169,regX:10.4,regY:33.5,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:7.5,x:254.3,y:174.3}},{t:this.instance_21,p:{regX:90.6,skewX:7.5,skewY:-172.5,x:327.3,y:180.3,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-15.7,skewY:164.3,y:272.3,x:290.6,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:7.5,x:278.1,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:10.9,regY:21.3,rotation:7.5,x:294.2,y:45.9,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:15.7,x:294.8,y:47,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:7.4,skewY:-172.6,x:321.5,y:49.9,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-1.4,skewY:179.7,x:320.7,y:50.8,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:7.5,x:299.9,y:15.4,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:7.5,skewY:-172.5,x:325.2,y:17.1,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.7,regY:27.4,rotation:7.5,x:299.8,y:96.9}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:11.5,skewY:-168.5,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:11,x:259.7,y:173.1}},{t:this.instance_21,p:{regX:90.7,skewX:11,skewY:-169,x:332.1,y:183.5,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.9,skewX:-15.9,skewY:164.1,y:272.5,x:290.6,regX:24.3,scaleX:1.032,scaleY:1.021}},{t:this.instance_18,p:{rotation:11,x:276.3,y:292.4,regX:67.8}},{t:this.instance_17,p:{regX:10.9,regY:21.3,rotation:11,x:307.5,y:47.5,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.4,rotation:19.3,x:307.9,y:48.5,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:11,skewY:-169,x:334.4,y:53.1,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:2.2,skewY:-176.8,x:333.6,y:53.9,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:11,x:315.1,y:17.3,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:11,skewY:-169,x:340.1,y:20.4,regY:4.7,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.7,regY:27.3,rotation:11,x:310,y:98.5}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:15,skewY:-165,regX:10.4,regY:33.5,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:7.5,x:254.3,y:174.3}},{t:this.instance_21,p:{regX:90.6,skewX:7.5,skewY:-172.5,x:327.3,y:180.3,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-15.7,skewY:164.3,y:272.3,x:290.6,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:7.5,x:278.1,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:10.9,regY:21.3,rotation:7.5,x:294.2,y:45.9,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:15.7,x:294.8,y:47,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:7.4,skewY:-172.6,x:321.5,y:49.9,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-1.4,skewY:179.7,x:320.7,y:50.8,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:7.5,x:299.9,y:15.4,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:7.5,skewY:-172.5,x:325.2,y:17.1,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.7,regY:27.4,rotation:7.5,x:299.8,y:96.9}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:11.5,skewY:-168.5,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},4).to({state:[{t:this.instance_22,p:{rotation:5.5,x:250.6,y:175.2}},{t:this.instance_21,p:{regX:90.6,skewX:5.5,skewY:-174.5,x:323.8,y:178.6,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-12.5,skewY:167.5,y:272.4,x:290.7,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:5.5,x:278.6,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.3,rotation:5.5,x:286.1,y:45.5,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.4,rotation:13.7,x:286.6,y:46.6,regY:7.9,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:5.4,skewY:-174.6,x:313.4,y:48.6,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-3.4,skewY:177.7,x:312.7,y:49.4,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:5.5,x:290.7,y:14.8,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:5.5,skewY:-174.5,x:315.9,y:15.6,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:5.5,x:293.5,y:96.2}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:11,skewY:-169,regX:10.4,regY:33.5,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:3.4,x:246.4,y:176.3}},{t:this.instance_21,p:{regX:90.6,skewX:3.4,skewY:-176.6,x:319.7,y:177.1,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.6,skewX:-8,skewY:172,y:272.4,x:290.6,regX:24.4,scaleX:1,scaleY:1}},{t:this.instance_18,p:{rotation:3.4,x:278.6,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:3.4,x:277.3,y:45.5,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:11.7,x:277.9,y:46.4,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:3.4,skewY:-176.6,x:304.8,y:47.5,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-5.4,skewY:175.6,x:304,y:48.3,regY:8.2,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:3.4,x:280.8,y:14.5,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:3.4,skewY:-176.6,x:306.1,y:14.4,regY:4.8,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:3.4,x:286.5,y:95.8}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:3.7,skewY:-176.3,regX:10.5,regY:33.4,x:326,y:333.4}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-1.8,x:236.7,y:179.7}},{t:this.instance_21,p:{regX:90.6,skewX:-1.8,skewY:178.2,x:309.8,y:173.8,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-1.8,x:279.3,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-1.8,x:255.5,y:46.6,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:6.5,x:256.2,y:47.5,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:-1.8,skewY:178.2,x:283.1,y:46,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-10.6,skewY:170.4,x:282.3,y:47.1,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-1.8,x:256.1,y:15.4,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-1.8,skewY:178.2,x:281.3,y:13.1,regY:4.9,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-1.8,x:269.3,y:95.9}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-6,x:229.1,y:182.9}},{t:this.instance_21,p:{regX:90.6,skewX:-6,skewY:174,x:301.6,y:171.8,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.8,regY:6.7,rotation:4.5,x:264.4,y:271.4}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-6,x:279.9,y:292.1,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-6,x:238.1,y:48.9,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:2.2,x:238.9,y:49.7,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:-6,skewY:174,x:265.6,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-14.9,skewY:166.2,x:264.9,y:47.3,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-6,x:236.4,y:17.8,regY:4.9,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-6,skewY:174,x:261.3,y:13.5,regY:4.9,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-6,x:255.4,y:97}},{t:this.instance_10,p:{regY:22.2,rotation:-8.7,y:320.7,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-10,x:221.9,y:186.6}},{t:this.instance_21,p:{regX:90.5,skewX:-10,skewY:170,x:293.5,y:170.5,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.9,regY:6.8,rotation:7.5,x:264.5,y:271.6}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-10,x:280.1,y:292.1,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-10,x:221.6,y:52.3,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:-1.7,x:222.5,y:53.1,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.3,skewX:-10,skewY:170,x:249,y:47.9,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-18.8,skewY:162.2,x:248.3,y:48.9,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-10,x:217.8,y:21.4,regY:4.9,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-10,skewY:170,x:242.5,y:15.4,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-10,x:242.3,y:99.1}},{t:this.instance_10,p:{regY:22.2,rotation:-14.2,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-13,x:217.5,y:190}},{t:this.instance_21,p:{regX:90.6,skewX:-13,skewY:167,x:288,y:170.1,regY:12.3,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.7,rotation:10.3,x:264.1,y:271.4}},{t:this.instance_19,p:{regY:6.9,skewX:-0.9,skewY:179.1,y:272.1,x:293.1,regX:24.3,scaleX:1.032,scaleY:1.021}},{t:this.instance_18,p:{rotation:-13,x:281,y:292.2,regX:67.8}},{t:this.instance_17,p:{regX:10.9,regY:21.4,rotation:-13,x:210,y:55.9,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:-4.7,x:211,y:56.6,regY:7.9,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.5,skewX:-13,skewY:167,x:237,y:50,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-21.8,skewY:159.2,x:236.6,y:51,regY:8.2,regX:3,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-13,x:204.7,y:25.2,regY:4.9,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-13,skewY:167,x:229,y:17.8,regY:4.8,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.3,rotation:-13,x:233.2,y:101.4}},{t:this.instance_10,p:{regY:22.1,rotation:-21.4,y:320.5,x:236.9}},{t:this.instance_9,p:{skewX:0,skewY:180,regX:10.4,regY:33.5,x:326.1,y:333.6}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-10,x:221.9,y:186.6}},{t:this.instance_21,p:{regX:90.5,skewX:-10,skewY:170,x:293.5,y:170.5,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.9,regY:6.8,rotation:7.5,x:264.5,y:271.6}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-10,x:280.1,y:292.1,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-10,x:221.6,y:52.3,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:-1.7,x:222.5,y:53.1,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.3,skewX:-10,skewY:170,x:249,y:47.9,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-18.8,skewY:162.2,x:248.3,y:48.9,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-10,x:217.8,y:21.4,regY:4.9,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-10,skewY:170,x:242.5,y:15.4,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-10,x:242.3,y:99.1}},{t:this.instance_10,p:{regY:22.2,rotation:-14.2,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},4).to({state:[{t:this.instance_22,p:{rotation:-6,x:229.1,y:182.9}},{t:this.instance_21,p:{regX:90.6,skewX:-6,skewY:174,x:301.6,y:171.8,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.8,regY:6.7,rotation:4.5,x:264.4,y:271.4}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-6,x:279.9,y:292.1,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-6,x:238.1,y:48.9,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:2.2,x:238.9,y:49.7,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:-6,skewY:174,x:265.6,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-14.9,skewY:166.2,x:264.9,y:47.3,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-6,x:236.4,y:17.8,regY:4.9,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-6,skewY:174,x:261.3,y:13.5,regY:4.9,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-6,x:255.4,y:97}},{t:this.instance_10,p:{regY:22.2,rotation:-8.7,y:320.7,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-1.8,x:236.7,y:179.7}},{t:this.instance_21,p:{regX:90.6,skewX:-1.8,skewY:178.2,x:309.8,y:173.8,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-1.8,x:279.3,y:292.3,regX:67.8}},{t:this.instance_17,p:{regX:11,regY:21.4,rotation:-1.8,x:255.5,y:46.6,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.5,rotation:6.5,x:256.2,y:47.5,regY:7.8,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.4,skewX:-1.8,skewY:178.2,x:283.1,y:46,regY:21.6,scaleY:0.991}},{t:this.instance_14,p:{skewX:-10.6,skewY:170.4,x:282.3,y:47.1,regY:8.3,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-1.8,x:256.1,y:15.4,regY:4.8,regX:5.4,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-1.8,skewY:178.2,x:281.3,y:13.1,regY:4.9,regX:5.4,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:37.8,regY:27.4,rotation:-1.8,x:269.3,y:95.9}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.5,skewX:-0.3,skewY:179.7,x:312.8,y:174.7,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_17,p:{regX:11.1,regY:21.4,rotation:-0.3,x:261.8,y:46.1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.6,rotation:7.9,x:262.5,y:47.1,regY:7.9,scaleX:1,scaleY:1,skewX:0,skewY:0}},{t:this.instance_15,p:{regX:10.3,skewX:-0.3,skewY:179.7,x:289.4,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-9.2,skewY:171.9,x:288.7,y:47.3,regY:8.4,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-0.3,x:263.3,y:15,regY:4.9,regX:5.5,scaleY:1,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-0.3,skewY:179.7,x:288.6,y:13.3,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.5,skewX:-0.3,skewY:179.7,x:312.8,y:174.7,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_17,p:{regX:11.2,regY:21.5,rotation:-0.3,x:261.9,y:46.3,scaleY:0.938,skewX:0,skewY:0}},{t:this.instance_16,p:{regX:3.6,rotation:0,x:262.5,y:47.2,regY:8,scaleX:0.999,scaleY:0.939,skewX:8.5,skewY:7.5}},{t:this.instance_15,p:{regX:10.3,skewX:-0.3,skewY:179.7,x:289.4,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-9.2,skewY:171.9,x:288.7,y:47.3,regY:8.4,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:-0.3,x:259.9,y:19,regY:7.7,regX:2.1,scaleY:0.938,skewX:0,skewY:0,scaleX:1}},{t:this.instance_12,p:{skewX:-0.3,skewY:179.7,x:288.9,y:14.4,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},5).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.5,skewX:-0.3,skewY:179.7,x:312.8,y:174.7,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_17,p:{regX:11.2,regY:21.5,rotation:0,x:261.9,y:46.5,scaleY:0.792,skewX:-0.4,skewY:-0.3}},{t:this.instance_16,p:{regX:3.6,rotation:0,x:262.5,y:47.3,regY:8.1,scaleX:0.996,scaleY:0.797,skewX:10,skewY:6.3}},{t:this.instance_15,p:{regX:10.3,skewX:-0.3,skewY:179.7,x:289.4,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-9.2,skewY:171.9,x:288.7,y:47.3,regY:8.4,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:0,x:259.9,y:23.4,regY:7.7,regX:2.1,scaleY:0.792,skewX:-0.4,skewY:-0.3,scaleX:1}},{t:this.instance_12,p:{skewX:-0.3,skewY:179.7,x:288.9,y:14.4,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.5,skewX:-0.3,skewY:179.7,x:312.8,y:174.7,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_17,p:{regX:11.2,regY:21.5,rotation:0,x:261.9,y:45.5,scaleY:0.665,skewX:-0.5,skewY:-0.2}},{t:this.instance_16,p:{regX:3.6,rotation:0,x:262.5,y:46.2,regY:8.1,scaleX:0.995,scaleY:0.672,skewX:11.9,skewY:5.3}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:44,regY:21.8,scaleY:0.91}},{t:this.instance_14,p:{skewX:-10,skewY:172.5,x:288.7,y:44.8,regY:8.4,regX:3.1,scaleX:1.053,scaleY:0.913}},{t:this.instance_13,p:{rotation:0,x:259.9,y:26.1,regY:7.7,regX:2.1,scaleY:0.665,skewX:-0.5,skewY:-0.2,scaleX:1}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:14.6,regY:5,regX:5.3,scaleY:0.918,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.3,skewX:-24.8,skewY:155.2,x:312.9,y:174.8,regY:12.6,scaleX:0.747,scaleY:1.125,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:45.1,regY:21.7,scaleY:0.835}},{t:this.instance_14,p:{skewX:-10.8,skewY:173.1,x:288.7,y:45.9,regY:8.4,regX:3.1,scaleX:1.052,scaleY:0.841}},{t:this.instance_13,p:{rotation:0,x:260.1,y:31.1,regY:7.8,regX:2.3,scaleY:0.433,skewX:-0.7,skewY:-0.1,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:18.2,regY:5,regX:5.3,scaleY:0.843,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_23,p:{regX:29.2,regY:10.5,scaleX:0.457,scaleY:1.323,rotation:-20,x:259.4,y:47.3}}]},1).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:35.5,scaleX:0.404,scaleY:0.584,skewX:-146,skewY:34,x:336.4,y:178.9,regX:64.5}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:44.5,regY:21.9,scaleY:0.868}},{t:this.instance_14,p:{skewX:-10.4,skewY:172.9,x:288.7,y:45.4,regY:8.6,regX:3.1,scaleX:1.052,scaleY:0.872}},{t:this.instance_13,p:{rotation:0,x:260.8,y:39.4,regY:22.3,regX:7.9,scaleY:0.278,skewX:5.9,skewY:6.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:16.5,regY:5.1,regX:5.3,scaleY:0.875,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_23,p:{regX:28.8,regY:10.3,scaleX:0.451,scaleY:0.709,rotation:-5,x:259.1,y:45}},{t:this.instance_24}]},1).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:35.3,scaleX:0.76,scaleY:0.599,skewX:-158.7,skewY:21.3,x:346.3,y:180.3,regX:64.5}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:45.2,regY:21.8,scaleY:0.851}},{t:this.instance_14,p:{skewX:-10.6,skewY:173,x:288.7,y:45.9,regY:8.4,regX:3.1,scaleX:1.052,scaleY:0.856}},{t:this.instance_13,p:{rotation:0,x:260.2,y:34.6,regY:8.2,regX:2.4,scaleY:0.278,skewX:28.9,skewY:29.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:17.9,regY:5.1,regX:5.3,scaleY:0.858,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_27,p:{x:258.9}},{t:this.instance_26,p:{scaleX:1.093,scaleY:0.558,x:417.3,y:174.1}}]},1).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:22.9,scaleX:0.759,scaleY:0.599,skewX:-166.5,skewY:13.5,x:313.4,y:174.6,regX:23.3}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:45.2,regY:21.8,scaleY:0.851}},{t:this.instance_14,p:{skewX:-10.6,skewY:173,x:288.7,y:45.9,regY:8.4,regX:3.1,scaleX:1.052,scaleY:0.856}},{t:this.instance_13,p:{rotation:0,x:260.2,y:34.6,regY:8.2,regX:2.4,scaleY:0.278,skewX:28.9,skewY:29.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:17.9,regY:5.1,regX:5.3,scaleY:0.858,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_27,p:{x:258.9}},{t:this.instance_26,p:{scaleX:0.981,scaleY:0.726,x:414.7,y:156.5}}]},1).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:22.9,scaleX:0.759,scaleY:0.599,skewX:-170.7,skewY:9.3,x:313.4,y:174.6,regX:23.4}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:45.2,regY:21.8,scaleY:0.851}},{t:this.instance_14,p:{skewX:-10.6,skewY:173,x:288.7,y:45.9,regY:8.4,regX:3.1,scaleX:1.052,scaleY:0.856}},{t:this.instance_13,p:{rotation:0,x:260.2,y:34.6,regY:8.2,regX:2.4,scaleY:0.278,skewX:28.9,skewY:29.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:17.9,regY:5.1,regX:5.3,scaleY:0.858,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_27,p:{x:258.9}},{t:this.instance_28}]},1).to({state:[{t:this.instance_29},{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.2,skewX:0.7,skewY:-179.2,x:289.4,y:44.9,regY:21.9,scaleY:0.813}},{t:this.instance_14,p:{skewX:-10,skewY:174.4,x:288.6,y:45.7,regY:8.6,regX:3.1,scaleX:1.051,scaleY:0.819}},{t:this.instance_13,p:{rotation:0,x:288.9,y:17.7,regY:5.1,regX:5.3,scaleY:0.82,skewX:0.7,skewY:-179.2,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_12,p:{skewX:28.9,skewY:29.9,x:258.9,y:34.6,regY:8.2,regX:2.4,scaleY:0.278,scaleX:0.946}},{t:this.instance_27,p:{x:258.8}}]},1).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:22.9,scaleX:0.759,scaleY:0.599,skewX:-170.7,skewY:9.3,x:313.4,y:174.6,regX:23.4}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:45.2,regY:21.8,scaleY:0.851}},{t:this.instance_14,p:{skewX:-10.6,skewY:173,x:288.7,y:45.9,regY:8.4,regX:3.1,scaleX:1.052,scaleY:0.856}},{t:this.instance_13,p:{rotation:0,x:260.2,y:34.6,regY:8.2,regX:2.4,scaleY:0.278,skewX:28.9,skewY:29.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:17.9,regY:5.1,regX:5.3,scaleY:0.858,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_27,p:{x:258.9}},{t:this.instance_28}]},22).to({state:[{t:this.instance_21,p:{regX:90.7,skewX:0,skewY:0,x:239.5,y:178.7,regY:12.3,scaleX:1,scaleY:1,rotation:-0.3}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_25,p:{regY:35.5,scaleX:0.404,scaleY:0.584,skewX:-146,skewY:34,x:336.4,y:178.9,regX:64.5}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_15,p:{regX:10.3,skewX:-0.4,skewY:179.7,x:289.4,y:44.5,regY:21.9,scaleY:0.868}},{t:this.instance_14,p:{skewX:-10.4,skewY:172.9,x:288.7,y:45.4,regY:8.6,regX:3.1,scaleX:1.052,scaleY:0.872}},{t:this.instance_13,p:{rotation:0,x:260.8,y:39.4,regY:22.3,regX:7.9,scaleY:0.278,skewX:5.9,skewY:6.9,scaleX:0.946}},{t:this.instance_12,p:{skewX:-0.4,skewY:179.7,x:288.9,y:16.5,regY:5.1,regX:5.3,scaleY:0.875,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_23,p:{regX:28.8,regY:10.3,scaleX:0.451,scaleY:0.709,rotation:-5,x:259.1,y:45}},{t:this.instance_24}]},1).to({state:[{t:this.instance_22,p:{rotation:-0.3,x:239.5,y:178.7}},{t:this.instance_21,p:{regX:90.5,skewX:-0.3,skewY:179.7,x:312.8,y:174.7,regY:12.4,scaleX:1,scaleY:1,rotation:0}},{t:this.instance_20,p:{regX:24.6,regY:6.6,rotation:0.5,x:264.2,y:271.3}},{t:this.instance_19,p:{regY:6.7,skewX:-1.8,skewY:178.2,y:272.4,x:290.6,regX:24.3,scaleX:1.027,scaleY:1.001}},{t:this.instance_18,p:{rotation:-0.3,x:279.4,y:292.3,regX:68}},{t:this.instance_17,p:{regX:11.2,regY:21.5,rotation:0,x:261.9,y:46.5,scaleY:0.792,skewX:-0.4,skewY:-0.3}},{t:this.instance_16,p:{regX:3.6,rotation:0,x:262.5,y:47.3,regY:8.1,scaleX:0.996,scaleY:0.797,skewX:10,skewY:6.3}},{t:this.instance_15,p:{regX:10.3,skewX:-0.3,skewY:179.7,x:289.4,y:46.3,regY:21.7,scaleY:0.991}},{t:this.instance_14,p:{skewX:-9.2,skewY:171.9,x:288.7,y:47.3,regY:8.4,regX:3.1,scaleX:1.055,scaleY:0.993}},{t:this.instance_13,p:{rotation:0,x:259.9,y:23.4,regY:7.7,regX:2.1,scaleY:0.792,skewX:-0.4,skewY:-0.3,scaleX:1}},{t:this.instance_12,p:{skewX:-0.3,skewY:179.7,x:288.9,y:14.4,regY:5,regX:5.3,scaleY:1,scaleX:1.104}},{t:this.instance_11,p:{regX:38,regY:27.4,rotation:-0.3,x:274.4,y:95.7}},{t:this.instance_10,p:{regY:22.1,rotation:0,y:320.6,x:236.8}},{t:this.instance_9,p:{skewX:-3.2,skewY:176.8,regX:10.4,regY:33.4,x:326.1,y:333.5}},{t:this.instance_8},{t:this.instance_7}]},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.8,190.3,647.3,528.7);
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFF00",
	opacity: 0.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;