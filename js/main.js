$(document).ready(function(){
 /*function animacionLogo(){
   var estado=0;
      if(estado===0){
        $(".logoimagen").addClass("animated");
        estado=1;
      }
      else{
        $(".logoimagen").removeClass("animated");
        estado=0;
      }
  };
  setInterval(animacionLogo, 900);*/
    /*Preloader*/
    //start of loader styles
    function starterAnim(){
        $("#fakebg").addClass('loadedb');
        setTimeout(function(){ $("#header").css({display: 'block', opacity: '1'});}, 550);
        setTimeout(function(){$("#fakebg").css("display", "none");},550);
        setTimeout(function(){$("body").css({display:'none', opacity: '1'}).fadeTo("slow",1);},550)
        thedelay = 550;
        $('.mascota').delay(thedelay).animate({opacity: '1', left: '0'}, 550);
        thedelay +=650;
    }
   var tover = ['N.png','I.png','P.png','L.png','E.png','T.png','O.png','C.png','K.png']; //just
    all = tover.length;
    var counter = 0;
    setTimeout(function(){ $("#header").css("display", "none");});
    for ( var i=0; i < all; i++){
      var img = new Image();
      img.onload = function(){
        counter+=1;
        console.log($(this).attr('src')+' - cargada!');
        console.log("inner counter"+counter);
        if (counter == all){
          starterAnim();
          console.log("now is all charged");
        }
      }
      img.src="images/"+tover[i]; //here we define the path
    }
    console.log("all"+all);

  /*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        if (response == "llene"){
          message.html("Compruebe que no es un robot");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });

});
